var vue = new Vue({
    el: '#app',

    data: {
        tasks: [],
        formMessage: '',
        editTaskValue: '',
        editTaskId: '',
        newTask: ''
    },

    computed: {
        completions: function() {
            return this.tasks.filter(function(task) {
                return task.completed;
            });
        },
        remaining: function() {
            return this.tasks.filter(function(task) {
                return ! task.completed;
            });
        }
    },

    filters: {
        completedTasks: function(tasks) {
            return tasks.filter(function(task) {
                return task.completed;
            });
        },

        uncompletedTasks: function(tasks) {
            return tasks.filter(function(task) {
                return ! task.completed;
            });
        }
    },

    methods: {
        addTask: function(e) {
            e.preventDefault();

            if (this.newTask.length < 1) {
                this.formMessage = "You can't submit a form without a message";
                return;
            }

            $.post('/api/tasks', {
                body: this.newTask
            }).done(function(task) {
                if (task.status == 'success') {
                    console.log(task);
                    vue.tasks.push(task);
                } else {
                    vue.formMessage = task.reason;
                }
            });

            this.newTask = '';
            this.formMessage = '';
        },

        editTask: function(task) {
            this.tasks.forEach(function(obj) {
                if (obj.edit_mode == 1) {
                    obj.edit_mode = 0;
                }
            });

            task.edit_mode = 1;
            this.editTaskValue = task.body;
            this.editTaskId    = task.id;
        },

        saveEditTask: function(e) {
            e.preventDefault();

            var currentTask = null;

            this.tasks.forEach(function(task) {
                if (task.id == vue.editTaskId) {
                    currentTask = task;
                }
            })

            if (currentTask != null) {
                $.ajax({
                    url: '/api/tasks/' + currentTask.id,
                    type: 'PUT',
                    data: {
                        id: currentTask.id,
                        body: vue.editTaskValue
                    },
                    success: function(data) {
                        currentTask.edit_mode = 0;

                        if (data.status == 'success') {
                            currentTask.body = data.body;
                        } else {
                            vue.formMessage = data.reason;
                        }
                    }
                });
            }
        },

        toggleTaskState: function(task) {
            $.ajax({
                url: '/api/tasks/' + task.id + '/state',
                type: 'PUT',
                data: {
                    id: task.id,
                    state: task.completed
                },
                success: function(data) {
                    if (data.status == 'success') {
                        task.completed = data.state;
                    } else {
                        vue.formMessage = data.reason;
                    }
                }
            });
        },

        completeAll: function(e) {
            e.preventDefault();

            this.tasks.forEach(function(task) {
                if (!task.completed) {
                    vue.toggleTaskState(task);
                }
            });
        },

        removeTask: function(task) {
            $.ajax({
                url: '/api/tasks/' + task.id,
                type: 'DELETE',
                success: function(data) {
                    if (data.status == 'success') {
                        vue.tasks.$remove(task);
                    } else {
                        vue.formMessage = data.reason;
                    }
                }
            });
        },

        clearCompleted: function() {
            this.tasks = this.tasks.filter(function(task) {
                if (task.completed) {
                    vue.removeTask(task);
                }

                return ! task.completed;
            })
        }
    }
});

$(document).ready(function() {
    loadTasksFromAPI();

    setInterval(function() {
        loadTasksFromAPI();
    }, 1000);
});

function loadTasksFromAPI() {
    $.get('/api/tasks', function(data) {
        if (data.length < 1) {
            return;
        }

        data.forEach(function(task) {
            var found = false;
            var taskObj = {
                id: task.id,
                body: task.body,
                completed: task.completed,
                timestamp: task.timestamp,
                edit_mode: 0
            };

            vue.tasks.forEach(function(obj) {
                if (obj.id == task.id) {
                    found = true;

                    if(obj.edit_mode == 0) {
                        obj.body = taskObj.body;
                        obj.completed = taskObj.completed;
                        obj.timestamp = taskObj.timestamp;
                    }
                }
            });

            if (!found) {
                vue.$data.tasks.push(taskObj);
            }
        });
    });
}

