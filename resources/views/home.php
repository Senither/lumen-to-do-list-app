<html>
    <head>
        <title>To do list</title>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/theme/dark.css">
    </head>

    <body>

        <div class="container">
            <div id="app">
                <div class="row">
                    <div class="col-md-1"></div>

                    <div class="col-md-10">
                        <h1>To do list <small>For Alexis</small></h1>

                        <hr>

                        <form v-on="submit: addTask">
                            <label for="todo">What needs to get done?</label>
                            <div class="input-group">
                                <input v-model="newTask"
                                       v-el="newTask"
                                       class="form-control"
                                       placeholder="I need to.."
                                       autofocus require
                                >
                                <span class="input-group-btn">
                                    <button class="btn btn-success">Add Task</button>
                                    <button type="reset" v-on="click: completeAll"
                                            class="btn btn-default"
                                    >
                                        Complete All
                                    </button>
                                </span>
                            </div>

                            <div v-if="formMessage"
                               role="alert"
                               class="alert alert-danger"
                            >
                                {{ formMessage }}
                            </div>
                        </form>

                        <hr>

                        <div id="tasks">
                            <div v-if="remaining.length">
                                <h3>Remaining tasks <small>{{ remaining.length }} task{{ remaining.length == 1 ? null : 's' }} have yet to be completed</small></h3>

                                <ul class="list-group">
                                    <li v-repeat="task: tasks | uncompletedTasks"
                                        v-on="dblclick: editTask(task)"
                                        class="list-group-item"
                                    >
                                        <div v-show="task.edit_mode">
                                            <form v-on="submit: saveEditTask">
                                                <div class="input-group">
                                                    <input v-model="editTaskValue"
                                                           class="form-control"
                                                           placeholder="I need to.."
                                                           require
                                                    >
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-success save" style="padding: 6px; margin-top: 0px;">Save Task</button>
                                                    </span>
                                                </div>
                                            </form>
                                        </div>

                                        <div v-show="!task.edit_mode">
                                            <span>{{ task.body }}</span>

                                            <button class="btn btn-danger" v-on="click: removeTask(task)">&#10007;</button>
                                            <button class="btn btn-success" v-on="click: toggleTaskState(task)">&#10004;</button>

                                            <span class="timestamp text-muted hidden-xs">Last changed {{ task.timestamp }}</span>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div v-if="completions.length">
                                <h3>Completed tasks <small>{{ completions.length }} task{{ completions.length == 1 ? null : 's' }} have been completed</small></h3>

                                <button v-on="click: clearCompleted"
                                        class="btn btn-danger clearCompleted"
                                >
                                    Clear All Completed Tasks
                                </button>

                                <div class="clearfix"></div>

                                <ul class="list-group">
                                    <li v-repeat="task: tasks | completedTasks"
                                        class="list-group-item"
                                    >
                                        <span>{{ task.body }}</span>

                                        <button class="btn btn-danger"
                                                v-on="click: toggleTaskState(task)"
                                        >
                                            &#10007;
                                        </button>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <!-- <pre>{{ $data | json }}</pre> -->
                    </div>

                    <div class="col-md-1"></div>
                </div>
            </div>
        </div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/0.11.10/vue.min.js"></script>
        <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src="assets/js/main.js"></script>
    </body>
</html>
