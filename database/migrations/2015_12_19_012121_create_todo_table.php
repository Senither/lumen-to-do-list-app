<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTodoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        app('db')->update("CREATE TABLE `test`.`todo` (
                            `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
                            `body` VARCHAR(255) NOT NULL ,
                            `completed` TINYINT(1) NOT NULL DEFAULT '0' ,
                            `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ,
                            PRIMARY KEY (`id`) ) ENGINE = InnoDB;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
