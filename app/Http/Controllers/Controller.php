<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    protected function responseSuccess(array $data = array())
    {
        return $this->response($data);
    }

    protected function responseError($reason = array())
    {
        return $this->response(['reason' => $reason], 'error');
    }

    public function response(array $data = array(), $status = 'success')
    {
        return array_merge(['status' => $status], $data);
    }
}
