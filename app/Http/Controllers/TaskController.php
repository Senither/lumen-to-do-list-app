<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TaskController extends Controller
{
    /**
     * Lists all the todo lists as a json array.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = [];
        foreach (app('db')->select('SELECT * FROM `todo` ORDER BY `id` ASC') as $row) {
            $carbon = new \Carbon\Carbon($row->created_at);

            $items[] = [
                'id' => $row->id,
                'body' => $row->body,
                'completed' => $row->completed,
                'timestamp' => $carbon->diffForHumans(),
            ];
        }

        return $items;
    }

    /**
     * Creates a new todo item.
     *
     * @param  Illuminate\Http\Request   $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // Check if request contains body here..

        $pdo    = app('db')->connection()->getPdo();
        $status = app('db')->update("INSERT INTO `todo` SET `body` = ?", [$request->get('body')]);

        if (!$status) {
            return $this->responseError('Failed to store the row in the database!');
        }

        return $this->responseSuccess([
            'id'        => $pdo->lastInsertId(),
            'body'      => $request->get('body'),
            'completed' => false,
            'timestamp' => (new \Carbon\Carbon())->timestamp()
        ]);
    }

    /**
     * Deletes/Removes a todo list item from the database.
     *
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        // Check if row was deleted
        app('db')->delete('DELETE FROM `todo` WHERE `id` = ?', [$id]);

        return $this->responseSuccess();
    }

    /**
     * Updates an already existing record.
     *
     * @param  Illuminate\Http\Request   $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if (!$request->has('id', 'body')) {
            return $responseError('Missing arguments');
        }

        $result = app('db')->update('UPDATE `todo` SET `body` = ?, `created_at` = NOW() WHERE `id` = ?', [
            $request->get('body'),
            $request->get('id')
        ]);

        if ($result) {
            return $this->responseSuccess([
                'id' => $request->get('id'),
                'body' => $request->get('body')
            ]);
        }
        return $this->responseError('Nothing was changed upon saveing the record.');
    }

    public function toggleState(Request $request, $id)
    {
        if ($request->get('id') != $id) {
            return $this->responseError('The IDs of the items does not match.');
        }

        $completed = ($request->get('state', 1) == 1) ? 0 : 1;
        $status    = app('db')->update('UPDATE `todo` SET `completed` = ? WHERE `id` = ?', [
            $completed, $request->get('id')
        ]);

        if ($status) {
            return $this->responseSuccess([
                'id'    => $request->get('id'),
                'state' => $completed,
            ]);
        }
        return $this->responseError('Failed to change the stored state value in the database.');
    }
}
