<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return view('home');
});

// API Routes
$app->get(    'api/tasks', ['uses' => 'TaskController@index']);
$app->post(   'api/tasks', ['uses' => 'TaskController@create']);
$app->delete( 'api/tasks/{id}', ['uses' => 'TaskController@delete']);
$app->put(    'api/tasks/{id}', ['uses' => 'TaskController@update']);
$app->put(    'api/tasks/{id}/state', ['uses' => 'TaskController@toggleState']);
