<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use LoggerTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'todo';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['item'];

    /**
     * Converts our timestamps to carbon instances.
     *
     * @var bool
     */
    public $timestamps = true;
}
